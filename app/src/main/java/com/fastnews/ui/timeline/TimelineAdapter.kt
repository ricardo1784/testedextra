package com.fastnews.ui.timeline

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.fastnews.R
import com.fastnews.repository.model.PostVO
import kotlinx.android.synthetic.main.include_item_timeline_thumbnail.view.*


class TimelineAdapter(val onClickItem: (PostVO, ImageView) -> Unit) :
    PagedListAdapter<PostVO,TimelineItemViewHolder>(diffCallback) {




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TimelineItemViewHolder =
        TimelineItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_timeline,
                parent,
                false
            )
        )


    override fun onBindViewHolder(holder: TimelineItemViewHolder, position: Int) {
        holder.data = getItem(position)
        holder.view.setOnClickListener {
            onClickItem(
                getItem(position)!!,
                holder.view.item_timeline_thumbnail
            )
        }
    }

    fun getLastItem(): PostVO? {
        return if(itemCount>0){
            getItem(itemCount-1)
        }else{
            null
        }

    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<PostVO>() {
            override fun areItemsTheSame(oldItem: PostVO, newItem: PostVO): Boolean =
                oldItem.id == newItem.id


            override fun areContentsTheSame(oldItem: PostVO, newItem: PostVO): Boolean =
                oldItem == newItem
        }
    }
}