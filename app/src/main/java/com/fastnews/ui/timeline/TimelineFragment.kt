package com.fastnews.ui.timeline

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.paging.PagedList
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.fastnews.R
import com.fastnews.mechanism.VerifyNetworkInfo
import com.fastnews.repository.model.PostResult
import com.fastnews.repository.model.PostVO
import com.fastnews.viewmodel.TimeLineViewModel
import com.paginate.Paginate
import kotlinx.android.synthetic.main.fragment_timeline.*
import kotlinx.android.synthetic.main.include_state_without_conn_timeline.view.*


class TimelineFragment : Fragment(), Paginate.Callbacks {

    private val viewModel: TimeLineViewModel by activityViewModels()
    private var hasLoadedAllItems = false
    private var isLoading: Boolean = false


    override fun isLoading(): Boolean = isLoading


    private lateinit var adapter: TimelineAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_timeline, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        state_without_conn_timeline.bt_try_again.setOnClickListener {
            verifyConnectionState()
        }

        offline_bar.setOnClickListener {
            verifyConnectionState()
        }

        buildActionBar()
        buildTimeline()
        verifyConnectionState()
    }

    private fun verifyConnectionState() {
        context.let {
            if (VerifyNetworkInfo.isConnected(it!!)) {
                hideNoConnectionState()
                showProgress()
                onLoadMore()
            } else {
                Toast.makeText(context, R.string.msg_no_connection, Toast.LENGTH_SHORT).show()
                hideProgress()
                showNoConnectionState()
            }
        }
    }

    private fun buildActionBar() {
        (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(false) // disable the button
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false) // remove the left caret
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowHomeEnabled(false)
        (activity as AppCompatActivity).supportActionBar?.title =
            resources.getString(R.string.app_name)
    }

    private fun buildTimeline() {
        adapter = TimelineAdapter { it, imageView ->
            onClickItem(it, imageView)
        }

        val layoutManager = LinearLayoutManager(context)

        timeline_rv.setHasFixedSize(true) //para melhorar o desempenho na renderização
        timeline_rv.layoutManager = layoutManager
        timeline_rv.itemAnimator = DefaultItemAnimator()
        timeline_rv.adapter = adapter

        Paginate.with(timeline_rv, this).setLoadingTriggerThreshold(2).build()

        viewModel.posts.observe(viewLifecycleOwner, Observer<PagedList<PostVO>> { posts ->
            if (posts.size > 0) {
                adapter.submitList(posts)
                hideProgress()
                hideNoConnectionState()
                showPosts()
            }

        })

        viewModel.postResultWeb.observe(viewLifecycleOwner, Observer<PostResult> { result ->

            isLoading = false
            hideProgress()

            if (!result.success && adapter.itemCount == 0) {
                showNoConnectionState()

            } else {
                if (!result.success && adapter.itemCount > 0) {
                    showNoConnectionState()
                    showPosts()

                } else {
                    showPosts()
                }
            }
        })

    }

    override fun onLoadMore() {
        if (!isLoading) {
            isLoading = true
            viewModel.loadPostFromNetwork()
        }

    }

    private fun showPosts() {
        timeline_rv.visibility = View.VISIBLE
    }

    private fun showProgress() {
        state_progress_timeline.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        state_progress_timeline.visibility = View.GONE
    }

    private fun showNoConnectionState() {
        if (adapter.itemCount > 0) {
            offline_bar.visibility = View.VISIBLE
        } else {
            state_without_conn_timeline.visibility = View.VISIBLE
        }
    }

    private fun hideNoConnectionState() {
        state_without_conn_timeline.visibility = View.GONE
        offline_bar.visibility = View.GONE
    }

    private fun onClickItem(postData: PostVO, imageView: ImageView) {
        val extras = FragmentNavigatorExtras(
            imageView to "thumbnail"
        )
        viewModel.select(postData)
        findNavController().navigate(R.id.action_timeline_to_detail, null, null, extras)
    }

    override fun hasLoadedAllItems(): Boolean = hasLoadedAllItems
}