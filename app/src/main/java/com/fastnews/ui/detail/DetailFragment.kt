package com.fastnews.ui.detail

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.transition.TransitionInflater
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.fastnews.ProjectConstants.PREFIX_HTTP
import com.fastnews.R
import com.fastnews.mechanism.TimeElapsed
import com.fastnews.mechanism.VerifyNetworkInfo
import com.fastnews.repository.model.CommentVO
import com.fastnews.repository.model.PostVO
import com.fastnews.ui.web.CustomTabsWeb
import com.fastnews.viewmodel.TimeLineViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_detail_post.*
import kotlinx.android.synthetic.main.include_detail_post_thumbnail.*
import kotlinx.android.synthetic.main.include_detail_post_title.*
import kotlinx.android.synthetic.main.include_item_timeline_ic_score.*
import kotlinx.android.synthetic.main.include_item_timeline_timeleft.*
import kotlinx.android.synthetic.main.include_state_without_conn_timeline.view.*

class DetailFragment : Fragment() {

    private var post: PostVO? = null

    private val commentViewModel: TimeLineViewModel by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detail_post, container, false)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buildActionBar()

        state_without_conn_detail_post.bt_try_again.setOnClickListener {
            verifyConnectionState()
        }

        commentViewModel.selected.observe(viewLifecycleOwner, Observer<PostVO> { postData ->
            post = postData
            populateUi()
        })

        commentViewModel.comments
            .observe(viewLifecycleOwner, Observer<List<CommentVO>> { comments ->
                comments.let {

                    if (comments.isEmpty() && !VerifyNetworkInfo.isConnected(requireContext())) {
                        hideStateProgress()
                        showNoConnectionState()
                    } else {
                        hideNoConnectionState()
                        populateComments(comments)
                        hideStateProgress()
                        showComments()
                    }
                }
            })
    }

    private fun buildActionBar() {
        val activity = activity as AppCompatActivity

        //activity.setSupportActionBar(toolbar)
        activity.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.detail_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }


    private fun populateUi() {

        populateAuthor()
        populateTimeLeftValue()
        populateTitle()
        populateThumbnail()
        buildOnClickDetailThumbnail()
        populateScore()
        showStateProgress()
        fetchComments()

    }

    private fun verifyConnectionState() {
        context.let {
            if (VerifyNetworkInfo.isConnected(it!!)) {
                hideNoConnectionState()
                showStateProgress()
                fetchComments()
            } else {
                Toast.makeText(context, R.string.msg_no_connection, Toast.LENGTH_SHORT).show()
                hideStateProgress()
                showNoConnectionState()
            }
        }
    }

    private fun fetchComments() {
        commentViewModel.loadComments(postId = post!!.id)
    }

    private fun populateComments(comments: List<CommentVO>) {
        if (isAdded) {
            activity?.runOnUiThread {
                detail_post_comments.removeAllViews()

                for (comment in comments) {
                    val itemReview = CommentItem.newInstance(requireActivity(), comment)
                    detail_post_comments.addView(itemReview)
                }
            }
        }
    }

    private fun showComments() {
        detail_post_comments.visibility = View.VISIBLE
    }

    private fun hideStateProgress() {
        state_progress_detail_post_comments.visibility = View.GONE
    }

    private fun showStateProgress() {
        state_progress_detail_post_comments.visibility = View.VISIBLE
    }

    private fun showNoConnectionState() {
        state_without_conn_detail_post.visibility = View.VISIBLE
    }

    private fun hideNoConnectionState() {
        state_without_conn_detail_post.visibility = View.GONE
    }

    private fun populateAuthor() {
        post?.author.let {
            item_timeline_author.text = it

            (activity as AppCompatActivity).supportActionBar?.title = it
        }
    }

    private fun populateTimeLeftValue() {
        post?.created_utc.let {
            val elapsed = TimeElapsed.getTimeElapsed(it!!.toLong(), requireContext())
            item_timeline_timeleft.text = elapsed
        }
    }

    private fun populateTitle() {
        post?.title.let {
            item_detail_post_title.text = it
        }
    }

    private fun populateThumbnail() {

        var thumbnailUrl = ""

        if (!TextUtils.isEmpty(post?.previewUrl) && post?.previewUrl!!.startsWith(PREFIX_HTTP)) {
            thumbnailUrl = post!!.previewUrl!!
        }

        if (!TextUtils.isEmpty(thumbnailUrl)) {
            Glide.with(item_detail_post_thumbnail.context)
                .load(thumbnailUrl)
                .placeholder(R.drawable.ic_placeholder)
                .into(item_detail_post_thumbnail)
            item_detail_post_thumbnail.visibility = View.VISIBLE
        }
    }

    private fun buildOnClickDetailThumbnail() {
        item_detail_post_thumbnail.setOnClickListener {
            if (!post?.url.isNullOrEmpty()) {
                context.let {
                    val customTabsWeb = CustomTabsWeb(requireContext(), post?.url!!)
                    customTabsWeb.openUrlWithCustomTabs()
                }
            } else {
                Snackbar.make(
                    item_detail_post_thumbnail,
                    R.string.error_detail_post_url,
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun populateScore() {
        post?.score.let {
            item_timeline_bt_score_text.text = it.toString()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> findNavController().navigateUp()
            R.id.action_share -> sharePost()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun sharePost() {
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, post?.url.toString())
            putExtra(Intent.EXTRA_TITLE, post?.title)
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)
    }
}