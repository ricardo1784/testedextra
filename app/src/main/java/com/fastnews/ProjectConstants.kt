package com.fastnews

object ProjectConstants {

    const val limit = 15
    const val DATABASE_NAME = "newsfeed"
    const val PREFIX_HTTP = "http"

}