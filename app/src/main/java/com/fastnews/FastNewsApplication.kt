package com.fastnews

import android.app.Application

class FastNewsApplication : Application() {

    companion object {

        lateinit var instance: FastNewsApplication
            private set

        var heighItem: Float = 0f
        var density: Float = 0f
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        density = resources.displayMetrics.density
        heighItem = resources.getDimension(R.dimen.height_thumbnail_timeline)
    }

}