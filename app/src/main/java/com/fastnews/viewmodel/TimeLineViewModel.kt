package com.fastnews.viewmodel

import androidx.annotation.UiThread
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Config
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import com.fastnews.ProjectConstants
import com.fastnews.repository.CommentRepository
import com.fastnews.repository.PostRepository
import com.fastnews.repository.model.CommentVO
import com.fastnews.repository.model.DatabaseImpl
import com.fastnews.repository.model.PostResult
import com.fastnews.repository.model.PostVO
import kotlinx.coroutines.launch

class TimeLineViewModel : ViewModel() {
    //FIXME adicionar injeções do database e repositories
    var comments: MutableLiveData<List<CommentVO>> = MutableLiveData()
    var postResultWeb: MutableLiveData<PostResult> = MutableLiveData()
    var selected = MutableLiveData<PostVO>()


    val myPagingConfig = Config(
        pageSize = 10
    )

    var postDataSource: DataSource.Factory<Int, PostVO> =
        DatabaseImpl.get().postDao().getAll()

    var posts = LivePagedListBuilder(postDataSource, myPagingConfig).build()

    @UiThread
    fun loadComments(postId: String) {
        viewModelScope.launch {
            val it = CommentRepository.getComments(postId)
            comments.postValue(it)
        }
    }

    @UiThread
    fun loadPostFromNetwork() { //Adicionar o parametro que é a montagem do after.
        viewModelScope.launch {
            val it = PostRepository.getPosts(ProjectConstants.limit)
            postResultWeb.postValue(it)
        }
    }

    fun select(postData: PostVO) {
        comments = MutableLiveData()
        selected.value = postData
    }

}