package com.fastnews.service.response

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PostResponse(val data: PostDataChild)

@JsonClass(generateAdapter = true)
data class PostDataChild(val children: List<PostChildren>, val after: String?, val before: String?)

@JsonClass(generateAdapter = true)
data class PostChildren(val data: PostData)

@JsonClass(generateAdapter = true)
data class PostData(
    val id: String,
    val author: String = "",
    val thumbnail: String = "",
    val name: String = "",
    val num_comments: Int,
    val score: Int,
    val title: String = "",
    val created_utc: Long,
    val url: String,
    val preview: Preview? = null
)


@JsonClass(generateAdapter = true)
data class Preview(val images: List<PreviewImage>)

@JsonClass(generateAdapter = true)
data class PreviewImage(
    val id: String,
    val source: PreviewImageSource,
    val resolutions: List<PreviewImageSource>
)

@JsonClass(generateAdapter = true)
data class PreviewImageSource(val url: String, val width: Int, val height: Int)

