package com.fastnews.service.response

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CommentResponse(val data: CommentDataChild)

@JsonClass(generateAdapter = true)
data class CommentDataChild(val children: List<CommentChildren>)

@JsonClass(generateAdapter = true)
data class CommentChildren(val kind: String, val data: CommentData)

@JsonClass(generateAdapter = true)
data class CommentData(
    val id: String,
    val author: String,
    val body: String?,
    val name: String,
    val downs: Int,
    val ups: Int,
    val created_utc: Long
)