package com.fastnews.repository

import android.util.Log
import androidx.core.text.HtmlCompat
import com.fastnews.FastNewsApplication
import com.fastnews.repository.model.DatabaseImpl
import com.fastnews.repository.model.PostResult
import com.fastnews.repository.model.PostVO
import com.fastnews.service.api.RedditAPI
import com.fastnews.service.response.Preview

object PostRepository : BaseRepository() {

    suspend fun getPosts(limit: Int): PostResult {

        val postVo = DatabaseImpl.get().postDao().findOldest()
        val after = postVo?.after ?: ""



        try{
            val postResponse = safeApiCall(
                call = { RedditAPI.redditService.getPostList(after, limit).await() },
                errorMessage = "Error to fetching posts"
            )

            val listPost: MutableList<PostVO> = mutableListOf()
            postResponse?.data?.children?.map { postChildren ->
                listPost.add(with(postChildren.data) {
                    PostVO(
                        id,
                        author,
                        name,
                        thumbnail,
                        name,
                        num_comments,
                        score,
                        title,
                        created_utc,
                        url,
                        HtmlCompat.fromHtml(getBestUrl(preview), HtmlCompat.FROM_HTML_MODE_LEGACY)
                            .toString()
                    )
                })
            }

            DatabaseImpl.get().postDao().insertAll(listPost)
            val success = postResponse?.data!=null

            return PostResult( success)

        }catch (e :Exception){
            Log.e("Error", e.message)
            return PostResult( false)
        }







    }


    private fun getBestUrl(preview: Preview?): String {

        preview?.images?.get(0)?.let { it ->
            if ((it.source.height / FastNewsApplication.density) <= FastNewsApplication.heighItem) {
                return it.source.url
            }

            var url = it.source.url
            it.resolutions.forEach {
                if ((it.height / FastNewsApplication.density) <= FastNewsApplication.heighItem) {
                    url = it.url
                }
            }
            return url
        }

        return ""
    }
}