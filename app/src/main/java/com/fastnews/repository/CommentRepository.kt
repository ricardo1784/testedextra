package com.fastnews.repository

import android.util.Log
import com.fastnews.repository.model.CommentVO
import com.fastnews.repository.model.DatabaseImpl
import com.fastnews.service.api.RedditAPI

object CommentRepository : BaseRepository() {

    const val PREFIX_COMMENT = "t1"

    suspend fun getComments(postId: String): List<CommentVO> {

        try {
            val commentsResponse = safeApiCall(
                call = { RedditAPI.redditService.getCommentList(postId).await() },
                errorMessage = "Error to fetch comments from postId -> $postId"
            )

            val result = mutableListOf<CommentVO>()
            commentsResponse?.map { response ->
                response.data.children.map { data ->
                    if (data.kind == PREFIX_COMMENT) {
                        with(data.data){
                            result.add(CommentVO(id,author,body,name,downs,ups,created_utc,postId))
                        }

                    }
                }
            }

            DatabaseImpl.get().commentDao().insertAll(result)
        }catch (e :Exception){
            Log.e("Error",e.message)
        }

        return DatabaseImpl.get().commentDao().getAllByPostId(postId)

    }

}