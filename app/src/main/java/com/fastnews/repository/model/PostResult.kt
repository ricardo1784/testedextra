package com.fastnews.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey



data class PostResult(var success: Boolean)

@Entity(tableName="post")
data class PostVO(
    @PrimaryKey val id: String,
    val author: String = "",
    val after: String ="",
    val thumbnail: String = "",
    val name: String = "",
    val num_comments: Int,
    val score: Int,
    val title: String = "",
    val created_utc: Long,
    val url: String,
    val previewUrl: String? = null)

