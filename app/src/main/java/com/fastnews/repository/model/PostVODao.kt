package com.fastnews.repository.model

import androidx.paging.DataSource
import androidx.room.*

@Dao
interface PostVODao {

    @Query("SELECT * FROM post order by post.created_utc desc")
    fun getAll(): DataSource.Factory<Int,PostVO>

    @Query("SELECT * FROM post WHERE post.created_utc = (select min(created_utc) from post)")
    suspend fun findOldest(): PostVO?

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(post: MutableList<PostVO>)

    @Query("SELECT COUNT(*) FROM post")
    suspend fun getCount(): Int

    @Delete
    suspend fun delete(post: PostVO)

}