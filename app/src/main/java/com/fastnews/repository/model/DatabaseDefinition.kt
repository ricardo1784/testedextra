package com.fastnews.repository.model

import androidx.room.Database
import androidx.room.RoomDatabase


@Database(entities = [PostVO::class, CommentVO::class], version = 1, exportSchema = false)
abstract class DatabaseDefinition : RoomDatabase() {
    abstract fun postDao(): PostVODao
    abstract fun commentDao(): CommentVODao
}