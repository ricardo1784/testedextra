package com.fastnews.repository.model

import androidx.room.Room
import com.fastnews.FastNewsApplication
import com.fastnews.ProjectConstants.DATABASE_NAME

object DatabaseImpl {

    var db = Room.databaseBuilder(
        FastNewsApplication.instance,
        DatabaseDefinition::class.java, DATABASE_NAME
    ).build()

    fun get() : DatabaseDefinition{
       return db
    }

}