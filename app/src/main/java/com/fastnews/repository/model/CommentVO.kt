package com.fastnews.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName="comment")
data class CommentVO(@PrimaryKey val id: String, val author: String, val body: String?, val name: String, val downs: Int, val ups: Int, val created_utc: Long, val post_id: String)