package com.fastnews.repository.model

import androidx.room.*

@Dao
interface CommentVODao {

    @Query("SELECT * FROM comment where comment.post_id = :postId  order by comment.created_utc desc")
    suspend fun getAllByPostId(postId: String): List<CommentVO>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(post: List<CommentVO>)

    @Query("SELECT COUNT(*) FROM comment")
    suspend fun getCount(): Int

    @Delete
    suspend fun delete(comment: CommentVO)

}