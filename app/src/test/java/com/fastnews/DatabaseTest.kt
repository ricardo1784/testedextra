package com.fastnews

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.fastnews.repository.model.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class DatabaseTest {

    val testDispatcher = TestCoroutineDispatcher()
    lateinit var postDao: PostVODao
    lateinit var commentDao: CommentVODao
    lateinit var db: DatabaseDefinition


    @Before
    fun createDb() {
        Dispatchers.setMain(testDispatcher)
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, DatabaseDefinition::class.java
        ).build()
        postDao = db.postDao()
        commentDao = db.commentDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun write_post_and_assert_inserted_quantity() = runBlocking {
        val postList = getListPost()

        postDao.insertAll(postList)
        val count = postDao.getCount()

        assertThat(count, equalTo(postList.size))
    }

    @Test
    @Throws(Exception::class)
    fun recover_more_older_post_in_database() = runBlocking {
        val postList = getListPost()
        postDao.insertAll(postList)
        val postVO = postDao.findOldest()
        assertThat(postVO?.id, equalTo("a1"))
    }

    @Test
    @Throws(Exception::class)
    fun write_comments_and_assert_inserted_quantity() = runBlocking {
        val commentList = getListComment()

        commentDao.insertAll(commentList)
        val count = commentDao.getCount()

        assertThat(count, equalTo(commentList.size))
    }

    @Test
    @Throws(Exception::class)
    fun find_comments_by_post_id() = runBlocking {
        val commentList = getListComment()

        commentDao.insertAll(commentList)

        val result =  commentDao.getAllByPostId("a3")

        assertThat(result.size, equalTo(2))
    }



    private fun getListPost(): MutableList<PostVO> {
        return arrayListOf(
            PostVO("a1", "author", "a1", "", "a1", 1, 1, "a1", 1, "", ""),
            PostVO("a2", "author", "a2", "", "a2", 1, 1, "a2", 2, "", ""),
            PostVO("a3", "author", "a3", "", "a3", 1, 1, "a3", 3, "", "")
        )
    }

    private fun getListComment(): MutableList<CommentVO> {
        return arrayListOf(
            CommentVO("1", "a1", "body", "name", 1, 1, 1, "a1"),
            CommentVO("2", "a2", "body", "name", 1, 1, 2, "a2"),
            CommentVO("3", "a3", "body", "name", 1, 1, 3, "a3"),
            CommentVO("4", "a3", "body", "name", 1, 1, 3, "a3")
        )
    }

}